

let cubeValue = 2;
const getCube = `The cube of ${cubeValue} is ${cubeValue**3}` ;
console.log(getCube);


const myAddress = ["258", "Washington Ave NW", "California 90011"];
const [houseNumber, Road, State] = myAddress;
console.log(`I live at ${houseNumber} ${Road}, ${State}`);



const myAnimals = {
	nameAnimal: "Lolong",
	animalClass: "saltwater crocodile",
	animalWeight: 1075,
	animalHeightFT: 20,
	animalHeightIn: 3
}
const {nameAnimal, animalClass, animalWeight, animalHeightFT, animalHeightIn} = myAnimals;
console.log(`${nameAnimal} was a  ${animalClass}. He weighed at ${animalWeight} kgs with a measurement of ${animalHeightFT} ft ${animalHeightIn} in.`);




const theNumbers = [1, 2, 3, 4, 5];

	// Arrow Function 
	theNumbers.forEach((theNumbers)=>{
		console.log(`${theNumbers}`);
	})

let reducedArray = theNumbers.reduce((acc, cur) => {
    return acc + cur;
});
console.log(reducedArray);2



	class dogDetail{
		constructor(name, age, breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
	}
	const dogAdmit = new dogDetail();

	// Reassign value of each properties 
	dogAdmit.name = "Frankie";
	dogAdmit.age = 5;
	dogAdmit.breed = "Miniature Dachshund";

	console.log(dogAdmit);
